package org.acme.getting.started;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import java.net.http.HttpClient;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.HashMap;


@Path("/message")
public class MessageIdentifyService {

        @POST
        @Path("identify")
        public Response identifyMessage(String input) {
            String fileType="";
            String urlToSend;
            try {
                    System.out.println(input);
                    if(input.contains("{") && input.contains("resourceType")){
                        fileType = invokeFHIR(input, fileType);
                        System.out.println("File type is " + fileType);
                    }
                    else if(input.contains("MSH")) {
                        System.out.println("The received file is of type HL7");
                        fileType = "HL7";
                        String hl7Message = input.substring(input.indexOf("MSH"), input.lastIndexOf("------"));
//                        urlToSend = "http://hl7-2-fhir-cdp.mdp-dev-857ec07bccf8d660b51a7713c074f306-0000.us-south.containers.appdomain.cloud/hl72fhirconverterservice-0.0.1-SNAPSHOT-2/rest/fhirConSer/HL7ToFHIR";
//                        urlToSend = urlToSend.concat("&");
//                        urlToSend = urlToSend.concat(hl7Message);
//                        System.out.println("URL"+ urlToSend);
                        //invokeHL7(urlToSend);
                        try{
                                 var values = new HashMap<String, String>() {{
                                    put("payload", hl7Message.toString());
                                }};
                                var objectMapper = new ObjectMapper();
                                String requestBody = objectMapper
                                        .writeValueAsString(values);
                        HttpClient client = HttpClient.newHttpClient();
                        HttpRequest request = HttpRequest.newBuilder()
                                .uri(URI.create("http://localhost:8081/hl72fhirconverterservice/rest/fhirConSer/HL7ToFHIR"))
                                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                                .build();

                        HttpResponse<String> response = client.send(request,
                                HttpResponse.BodyHandlers.ofString());
                        System.out.println(response.statusCode());

                        String responseBody = response.body();
                        System.out.println(responseBody);
                            if(responseBody.contains("{") && responseBody.contains("resourceType")){
                                fileType = invokeFHIR(input, fileType);


                            }
                    } catch(Exception e) {

                }
                    }
                    else {
                        System.out.println("Unknown file type");
                        fileType = "Unknown";
                    }
                    System.out.println("Done");

                } catch (Exception e) {
                    e.printStackTrace();
                }
              return Response.status(200)
                    .entity("IdentifyMessage is called, Received file is of type : " + fileType).build();

        }

    private String invokeFHIR(String input, String fileType) {
        String urlToSend;
        System.out.println("The received file is of type FHIR");
        JSONObject inputJsonObj = new JSONObject(input.substring(input.indexOf('{')));
        String resourceType = (String) inputJsonObj.get("resourceType");
        String resourceId = (String) inputJsonObj.get("id");
        System.out.println("Resource type is " + resourceType);
        if(resourceType.equalsIgnoreCase("allergyIntolerance")){
            fileType = "FHIR: allergyintolerance";
            urlToSend = "http://169.47.93.106:8080/iDAAS/allergyintolerance";
            invokeIDAAS(urlToSend);
        }
        else if(resourceType.equalsIgnoreCase("patient")){
            fileType = "FHIR: patient";
            urlToSend = "http://169.47.93.106:8080/iDAAS/patient";
            invokeIDAAS(urlToSend);
        }
        return fileType;
    }

    private void invokeIDAAS(String urlToSend) {
         StringBuilder builder = new StringBuilder();
         InputStreamReader in = null;
        String userPass;
        String message;
        try {
            URL url = new URL(urlToSend);
            userPass = "fhiruser:FHIRDeveloper123";

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userPass.getBytes()));
            urlConnection.setRequestProperty ("Authorization", basicAuth);
            if (urlConnection != null && urlConnection.getInputStream() != null) {
                in = new InputStreamReader(urlConnection.getInputStream(),
                        Charset.defaultCharset());
                BufferedReader bufferedReader = new BufferedReader(in);

                if (bufferedReader != null) {
                    int cp;
                    while ((cp = bufferedReader.read()) != -1) {
                        builder.append((char) cp);
                    }
                    bufferedReader.close();
                }
            }
            in.close();
            message =  builder.toString();
            System.out.println(message);
        } catch (Exception e) {
            throw new RuntimeException("Exception while calling URL:" + urlToSend, e);
        }

        System.out.println(message);
    }


   }

